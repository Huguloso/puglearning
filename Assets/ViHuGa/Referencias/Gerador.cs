﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gerador : MonoBehaviour {

	//conhecer o prefab do inimigo
	public GameObject inimigo;

	//contar tempo - temporizador / timer
	public float tempo_atual;
	//tempo criar
	public float tempo_cria = 2;

    //vetor para guardar objs
    public GameObject[] posicoes;


	// Use this for initialization
	void Start () {
        //localiza todos objs com tag - nome exato
        posicoes = GameObject.FindGameObjectsWithTag("Spawn");

	}
	
	// Update is called once per frame
	void Update () {

		//conta tempo
		tempo_atual += Time.deltaTime;
		//cada inteiro = 1 segundo
		if (tempo_atual >= tempo_cria) {
			//zerar o timer 
			tempo_atual = 0;

			//randomizar a posicao
			Vector3 pos;
            //pos.x = Random.Range (-50, 50);
            //pos.y = Random.Range (5, 10);
            //pos.z = Random.Range (-50, 50);

            //usar vetor de objs aux
            //randomiza de 0 a tamanho do vetor
            pos = posicoes[Random.Range(0, posicoes.Length)].transform.position;

			//criar instancia do inimigo
			Instantiate (inimigo, pos, transform.rotation);
		}

        //EX. remove inimigos
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            RemoveInimigos();
        }
	}

    //remove todos inimigos por tag
    void RemoveInimigos()
    {
        //encontra todos por tag
        GameObject[] inimigos = GameObject.FindGameObjectsWithTag("Inimigo");

        //percorrer o vetor
        for (int i =0; i < inimigos.Length; i++)
        {
            //apagar cada um 
            Destroy(inimigos[i]);
        }
    }


}
