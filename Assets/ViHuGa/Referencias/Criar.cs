﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Criar : MonoBehaviour {

	//pos aux de saida
	GameObject saida;

	//var guarda um prefab = e um GameObject = selecionar pelo inspector
	public GameObject prefab;

	// Use this for initialization
	void Start () {
		//quando liga - localiza obj saida
		saida = GameObject.Find("Saida");
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			//quem, posicao, e rotacao
			GameObject novo = Instantiate(prefab, saida.transform.position, saida.transform.rotation); 
		}


		if (Input.GetMouseButtonDown (1)) {
		//if (Input.GetMouseButton(0)) {
			//var guarda obj
			GameObject novo;
			//cria obj
			novo = GameObject.CreatePrimitive (PrimitiveType.Cube);
			//copia para minha pos
			//novo.transform.position = transform.position;
			//um pouco abaixo
			//novo.transform.position += new Vector3(0,-0.5f, 0);
			//copia pos do obj aux de saida
			novo.transform.position = saida.transform.position;
			//copia a rot do obj de saida
			novo.transform.rotation = saida.transform.rotation;

			//nome do novo obj
			novo.name = "NovoCubo";
			//mudar a cor
			novo.GetComponent<MeshRenderer>().material.color = Color.yellow;
			//adicionar um script / component
			//novo.AddComponent<Girar>();

			//adicionar fisica
			novo.AddComponent<Rigidbody>();
			//arremessar o obj - adicionar forca fisica
			//Y=cima, z+ = frente
			novo.GetComponent<Rigidbody>().AddRelativeForce(0,0,2500);

			//destruir em 2.5 segundos
			Destroy(novo, 2.5f);

			//adiciona script bala
			novo.AddComponent<Bala>();

		}
	}
}
