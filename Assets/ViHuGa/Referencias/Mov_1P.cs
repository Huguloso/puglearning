﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mov_1P : MonoBehaviour {

    //Vida Do Jogador
    public float Vida = 300;
    public float Vida_MAX = 300;

    public float xp = 0;
    public float xp_max = 100;
    public float xp_aux = 0;

    //Movimentos com A e D
    [SerializeField] private string horizontalInputName;
    //Movimentos com W e S
    [SerializeField] private string verticalInputName;
    //Define a velocidade de mivimento
    [SerializeField] private float Velocidade_de_Movimento = 13.0f;

    private CharacterController charController;

    //Usaremos AnimationCurve para expecificar como pulamos
    [SerializeField] private AnimationCurve jumpFallOff;
    //Um multiplicador para a curva
    [SerializeField] private float jumpMultiplier;
    //Determinamos uma variavel para ser o botão do pulo
    [SerializeField] private KeyCode jumpKey;



    //Uma variavel para dizer se estamos ou não pulando
    private bool isJumping;


    // Use this for initialization
    void Start()
    {

        charController = GetComponent<CharacterController>();

    }

    // Update is called once per frame
    void Update()
    {
        // --- Achando - as - Coisas --- //
        GameObject.Find("barra_de_xp").GetComponent<Image>().fillAmount = xp / xp_max;
        GameObject.Find("barra_de_vida").GetComponent<Image>().fillAmount = Vida / Vida_MAX;

        // --- Condicoes --- //

        // Vida Maxima
        if (Vida >= Vida_MAX)
        {
            Vida = Vida_MAX;
        }

        // Level UP
        if (xp >= Vida_MAX)
        {
            xp_aux = xp - xp_max;
            xp_max = xp_max * 2;
            xp = xp_aux;
            xp_aux = 0;
            Vida_MAX = Vida_MAX + 50;
            Vida = Vida_MAX;
        }


        // --- Botoes --- //

        // Retira Vida
        if (Input.GetKeyDown(KeyCode.I))
        {
            Vida = Vida - 10;
        }

        // Adiciona Vida
        if (Input.GetKeyDown(KeyCode.O))
        {
            Vida = Vida + 10;
        }

        // Adiciona XP
        if (Input.GetKeyDown(KeyCode.P))
        {
            xp = xp + 10;
        }



        PlayerMovement();
    }

    //Movimentação do Jogador, no caso caminhar e correr
    private void PlayerMovement()
    {
        float horizInput = Input.GetAxis(horizontalInputName) * Velocidade_de_Movimento;// * Time.DeltaTime; 
        float vertInput = Input.GetAxis(verticalInputName) * Velocidade_de_Movimento;// * Time.DeltaTime;

        //Se movimentar assim que apertar W e S
        Vector3 forwardMovement = transform.forward * vertInput;
        //Se movimentar assim que apertar A e D
        Vector3 rightMovement = transform.right * horizInput;

        charController.SimpleMove(forwardMovement + rightMovement);
        //O "SimpleMove" faz com que não necessite do Time.DeltaTime

        // R para correr
        if (Input.GetKey("r") || (Input.GetKey(KeyCode.LeftShift)))
        {
            charController.SimpleMove(forwardMovement + rightMovement);
        }

        //Chamamos o metodo de pular
        JumpInput();
    }

    //Essa funsão é usada para pular
    private void JumpInput()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    private IEnumerator JumpEvent()
    {
        charController.slopeLimit = 90;

        float timeInAir = 0.0f;

        do
        {
            float jumoForce = jumpFallOff.Evaluate(timeInAir);
            charController.Move(Vector3.up * jumoForce * jumpMultiplier * Time.deltaTime);

            timeInAir += Time.deltaTime;

            yield return null;
        } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);// fericia quando terminou de pular

        charController.slopeLimit = 45.0f;

        isJumping = false;
    }

    //Metodo para Levar Dano
    public void Levar_Dano(float Dano)
    {
        Vida -= Dano;
    }
}
