﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

	public float velocidade = 3000;

    //controle destroi apos som
    public bool destruir;

	//prefab efeito - setar via inspector
	public GameObject efeito;

	// Use this for initialization
	void Start () {

		//empurrao quando inicia
		GetComponent<Rigidbody>().AddRelativeForce(0,0,velocidade);

	}


    private void Update()
    {
        //ordem destruir
        if (destruir)
        {
            //se acabou o som
            if (!GetComponent<AudioSource>().isPlaying)
            {
                Destroy(gameObject);
            }

        }
    }

    //COLISAO FISICA- RigidBody
    void OnCollisionEnter(Collision collision) {

		//saber o nome de quem colidiu
		string nome = collision.gameObject.name;
		Debug.Log ("BALA bateu em : " + nome);

		//por tag
		if (collision.gameObject.tag == "Inimigo") {
			//DESTRUIR O OBJ Q BATER
			Destroy (collision.gameObject);
		}

        //CRIAR UMA COPIA DO PREFAB - efeito
        Instantiate(efeito, transform.position, transform.rotation);

        //AUTO DESTRUIR A BALA
        //Destroy(gameObject);

        //desliga visual
        GetComponent<Renderer>().enabled = false;
        //travar a fisica
        GetComponent<Rigidbody>().isKinematic = true;

        destruir = true;

	}


}
