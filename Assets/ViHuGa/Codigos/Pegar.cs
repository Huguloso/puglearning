﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pegar : MonoBehaviour {

    //Codigo do Dog

    //-----------------------------------------------------------------//

    //Vector3 para auxiliar na posição do objeto em relação a o carregador
    public Vector3 Pos_Aux;

    //Uma variavel boleana para dizer se o objeto esta ou não sendo carregado
    public bool Esta_Sendo_Carregado = false;
    
    //Uma variavel para o objeto que o carrega(O jogador)
    public GameObject Carregador;

    //Uma variavel para verificar se o Dog foi entregue
    public bool Entregue = false;

    //Variavel para a cor da coleira
    public int Cor;



    GameObject acerto, erro, pego; 
    
    
    
    
    
    // Use this for initialization
	void Start () {
        Carregador = GameObject.Find("Jogador");
    }









	
	// Update is called once per frame
	void Update () {
        //Se Esta sendo Carregado
        if (Esta_Sendo_Carregado)
        {
            //Define o Objeto como o filho de outro
            transform.SetParent(Carregador.transform);

            //Cria um vetor Para Posições
            Vector3 Posicao;

            //Define as posições de X, Y e Z
            Posicao.x = Pos_Aux.x;
            Posicao.y = Pos_Aux.y;
            Posicao.z = Pos_Aux.z;

            //Transforma a posição do objeto baseado no Vector3 Posicao
            transform.localPosition = Posicao;
        }

        //Se o Dog foi entregue
        if(Entregue)
        {
            




            Destroy(this.gameObject);
        }
    }

    public void Carregar_Dog()
    {
        //Se esta carregando o dog
        if(Esta_Sendo_Carregado == true)
        {
            //larga o dog
            Esta_Sendo_Carregado = false;
        }
        //Se não esta
        else
        {
            //Pega o dog
            Esta_Sendo_Carregado = true;
        }
    }

    public void Devolver_Dog()
    {
        //Entregamos o dog
        Entregue = true;
    }

}
