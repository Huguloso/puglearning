﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menus : MonoBehaviour {

    public string fase1 = "Jogo";
    public string ranking = "Ranking";
    public string tutorial = "Tutorial";
    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ComecarJogo()
    {
        SceneManager.LoadScene(fase1);
    }
    public void SairJogo()
    {
        Application.Quit();
    }
    public void Tutorial()
    {
        SceneManager.LoadScene(tutorial);
    }
}
