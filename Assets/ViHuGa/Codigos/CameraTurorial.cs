﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CameraTurorial : MonoBehaviour {

    public int parte = 0;
    public Animator anim;
    public float tempo = 5f;
    public bool apertavel = false;
    public GameObject continuar;
    public Text tutor;
    public string texto1, texto2, texto3;

    void Start()
    {
        continuar.SetActive(false);
        anim = GetComponent<Animator>();
        parte = 0;

        tutor.text = texto1;
    }

    // Update is called once per frame
    void Update () {

        if (tempo <= 0)
        {
            apertavel = true;
            continuar.SetActive(true);
        }
        else
        {
            tempo = tempo - Time.deltaTime;
            apertavel = false;
        }



        if (apertavel)
        {

            if (Input.anyKeyDown)
            {
                parte++;
                continuar.SetActive(false);
                tempo = tempo - Time.deltaTime;

                if (parte == 1)
                {
                    anim.SetTrigger("passo1");
                    tutor.text = texto2;
                }
                else if (parte == 2)
                {
                    anim.SetTrigger("passo2");
                    tutor.text = texto3;
                }
                else
                {
                    SceneManager.LoadScene("Menu");
                }

                tempo = 3f;

            }
        }

        



	}
}
