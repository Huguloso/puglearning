﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeradorDogs : MonoBehaviour {

    public GameObject dogR, dogG, dogB, dogA;
    [Range(0, 1000)] public int maxChance = 1000;
    public float tempo = 0;
    public bool criavel = true;

    Random rnd = new Random();

    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {


        // --- EXPLICACAO ---
        // Se o contador de tempo for menor ou igual a zero torna criavel
        // Se nao, torna nao criavel
        // Serve para poder dar um espaco de tempo entre a criacao de dogs

        if (tempo <= 0)
        {
            criavel = true;
        }
        else
        {
            tempo = tempo - Time.deltaTime;
            criavel = false;
        }


        // --- EXPLICACAO ---
        // Passo 1- Verifica se pode criar
        // Passo 2- Verifica a chece de criar ou nao um novo dog
        // Passo 3- Sorteia qual dog sera criado
        //
        if (criavel)
        {
            Criar();
        }

    }

    void Criar()
    {
        GameObject novoDog;
        int chance = Random.Range(0, maxChance);

        if (chance == 10)
        {
            int dog = Random.Range(0, 4);

            if (dog == 0)
            {
                novoDog = Instantiate(dogR, transform.position, transform.rotation);
            }
            else if (dog == 1)
            {
                novoDog = Instantiate(dogG, transform.position, transform.rotation);
            }
            else if (dog == 2)
            {
                novoDog = Instantiate(dogB, transform.position, transform.rotation);
            }
            else
            {
                novoDog = Instantiate(dogA, transform.position, transform.rotation);
            }

            tempo = 0.35f;


        }

    }
}