﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour
{

    public float dogSpeed;

    private float randomizedSpeed = 0f;
    private float nextActionTime = -1f;
    private Vector3 targetPosition;

    // 
    private void FixedUpdate()
    {
        if (dogSpeed > 0f)
        {
            Walk();
        }
    }

    private void Walk()
    {
        if (Time.fixedTime >= nextActionTime)
        {
            randomizedSpeed = dogSpeed * UnityEngine.Random.Range(.5f, 1.5f);

            targetPosition = AreaT.ChooseRandomPosition(transform.parent.position, 100f, 260f, 2f, 13f);

            transform.rotation = Quaternion.LookRotation(targetPosition - transform.position, Vector3.up);

            float timeToGetThere = Vector3.Distance(transform.position, targetPosition) / randomizedSpeed;
            nextActionTime = Time.fixedTime + timeToGetThere;
        }
        else
        {
            Vector3 moveVector = randomizedSpeed * transform.forward * Time.fixedDeltaTime;
            if (moveVector.magnitude <= Vector3.Distance(transform.position, targetPosition))
            {
                transform.position += moveVector;
            }
            else
            {
                transform.position = targetPosition;
                nextActionTime = Time.fixedTime;
            }
        }
    }
}
