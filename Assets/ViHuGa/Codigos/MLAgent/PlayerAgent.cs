﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class PlayerAgent : Agent
{
    public float moveSpeed = 10f;

    public AreaT playerArea;

    public GameObject entregarDogPrefab;

    private GameObject casa;

    private bool isFull;

    public float DeliveryDogRadius = 5f;

    CharacterController cc;

    Vector3 movement;

    public float maxtime = 20f;
    public float TimeRemaining;


    //encontrar objetos na cena
    //inicializado uma vez
    public override void Initialize()
    {
        base.Initialize();
        cc = GetComponent<CharacterController>();
        casa = playerArea.CasaEntrega;
        TimeRemaining = maxtime;
    }

    //recebe e responde os comandos
    //vectorAction: matriz de valores numericos correspondentes as ações do agente

    public override void OnActionReceived(float[] vectorAction)
    {
        //inicializa o x e o z com 0 (parado)
        float xMovement = 0.0f;
        float zMovement = 0.0f;

        float velocidade_mov = 5;

        if (vectorAction[0] == 1)
        {
            //vai para esquerda
            xMovement = -velocidade_mov;
        }
        else if (vectorAction[0] == 2)
        {
            //vai para direita
            xMovement = velocidade_mov;
        }

        if (vectorAction[1] == 1)
        {
            //vai para baixo
            zMovement = -velocidade_mov;
        }
        else if (vectorAction[1] == 2)
        {
            //vai para cima
            zMovement = velocidade_mov;
        }

        movement = new Vector3(xMovement, 0.0f, zMovement);

        cc.SimpleMove(movement);
        // Apply a tiny negative reward every step to encourage action
        if (MaxStep > 0) AddReward(-1f / MaxStep);

    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = 0f;
        actionsOut[1] = 0f;
        if (Input.GetKey(KeyCode.W))
        {
            // move forward
            actionsOut[0] = 2f;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            // move forward
            actionsOut[0] = 1f;
        }
        if (Input.GetKey(KeyCode.A))
        {
            // turn left
            actionsOut[1] = 1f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            // turn right
            actionsOut[1] = 2f;
        }

    }

    //redefinir area
    public void AgentReset()
    {
        isFull = false;
        playerArea.ResetarArea();
        DeliveryDogRadius = Academy.Instance.EnvironmentParameters.GetWithDefault("feed_radius", 6f);
        TimeRemaining = maxtime;
        
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        //entregou cachorro?
        sensor.AddObservation(isFull);

        //distancia até a casa
        sensor.AddObservation(Vector3.Distance(casa.transform.position, transform.position));

        //direção para a casa
        sensor.AddObservation((casa.transform.position - transform.position).normalized);

        //player voltando
        sensor.AddObservation(transform.forward);
    }

    private void FixedUpdate()
    {
        
        if(StepCount % 5 == 0)
        {
            RequestDecision();
        }
        else
        {
            RequestAction();
        }

        if(Vector3.Distance(transform.position, casa.transform.position) < DeliveryDogRadius)
        {
            EntregouCachorro();
        }
        else if (Vector3.Distance(transform.position, casa.transform.position) > 100)
        {
            AgentReset();
        }

        if(TimeRemaining > 0)
        {
            TimeRemaining -= Time.deltaTime;

            //Debug.Log(TimeRemaining);
        }
        else
        {
            AgentReset();
            EndEpisode();
        }

    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.CompareTag("Dog"))
        {
            PegouCachorro(collision.gameObject);
        }
        else if (collision.transform.CompareTag("Casa"))
        {
            EntregouCachorro();
        }
    }

    private void PegouCachorro(GameObject dogObject)
    {
        if (isFull) return; //n pode entregar outro cachorro
        isFull = true;
        playerArea.RemoveSpecificDog(dogObject);

        AddReward(1f);
    }

    private void EntregouCachorro()
    {
        //Debug.Log("entrou:" + isFull);
        if (!isFull) return; //nada para entregar
        isFull = false;

        AddReward(1f);

        if(playerArea.DogsRestantes <= 0)
        {
            AgentReset();
            EndEpisode();
        }
    }
}
