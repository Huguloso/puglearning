﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girar : MonoBehaviour {

    public int vel = 60;
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(0, vel * Time.deltaTime, 0);
	}
}
