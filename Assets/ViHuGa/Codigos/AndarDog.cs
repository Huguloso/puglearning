﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndarDog : MonoBehaviour {

    public float velocidade = 3;
    private Vector3 movimento;
    public CharacterController cc;
    public bool andar = true;
    public bool morre = false;
    public int direcao = 1;
    public float forca_gravitacional = 2;

    // Use this for initialization
    void Start()
    {

        if (gameObject.transform.position.x < 0)
        {
            direcao = -1;
            transform.Rotate(0, 90, 0);
        }
        else
        {
            transform.Rotate(0, 270, 0);
            direcao = 1;
        }

        cc = GetComponent<CharacterController>();

    }

    // Update is called once per frame
    void Update()
    {

        movimento = new Vector3(velocidade * direcao, movimento.y, movimento.z);

        if (andar == true)
        {

        }
        if (morre == true)
        {
            Destroy(gameObject);
        }

        movimento.y = movimento.y + (Physics.gravity.y * forca_gravitacional * Time.deltaTime);

        cc.Move(movimento * Time.deltaTime);



    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Toma toddynho");

        if (other.gameObject.tag == "Paredao")
        {
            
            morre = true;
        }
    }
}
