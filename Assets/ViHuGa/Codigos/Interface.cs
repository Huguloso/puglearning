﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour {

	// Use this for initialization
	

    public Text timerText;
    public float tempo_inicio;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        tempo_inicio -= Time.deltaTime;

        int sec = (int)(tempo_inicio % 60);
        int min = (int)(tempo_inicio / 60) % 60;

        string timerString = string.Format("{0:0}:{1:00}", min, sec);

        timerText.text = timerString;

        GameOver();
    }

    void GameOver()
    {
        if (tempo_inicio <= 0)
        {
            //Application.LoadLevel("Game Over");
        }
    }
}
