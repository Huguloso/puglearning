﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Peguei : MonoBehaviour
{
    //Codigo do Player
    
    //-----------------------------------------------------------------//

    //Cria uma viariavel se esta ou não carregando um objeto
    public bool Esta_Carregando = false;

    //Variavel para definir
    public GameObject Dog;

    //Variavel para a cor da coleira
    public int Cor;

    //Player Vida
    public int vida = 5;
    [Space(10)]
    public ParticleSystem FX_Feliz;
    public ParticleSystem FX_Triste;

    // Use this for initialization
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        //Se estiver Carregando
        if(Esta_Carregando == true)
        {
            //Cria uma variavel de componente de codigo Pegar
            Pegar pegar = Dog.GetComponent<Pegar>();

            //Define a captura a variavel de cor do objeto e passa para o player
            Cor = pegar.Cor;
        }
        //Se NÂO estiver Carregando
        else
        {
            //Definimos a cor para uma que não sera usado
            Cor = 666;
        }


    }




    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //string nome = hit.gameObject.name;
        //Debug.Log("Cao Bateu em: " + nome);

        //Se o jogador estiver Carregando um objeto
        if (Esta_Carregando == false)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                RaycastHit info;
                if (Physics.Linecast(transform.position, transform.position, out info))
                {
                    string nome = info.transform.name;
                    float distancia = info.distance;
                    Debug.Log("VISAO ROBO bateu em: " + nome + " distancia: " + distancia);
                }
            }


            if (hit.gameObject.tag == "DogMau")
            {

                GameObject.Find("Temporizador").GetComponent<Interface>().tempo_inicio -= 10f;


                Debug.Log("O DOG EH MAU!!");
                Destroy(hit.gameObject);
            }
            //Se encostar em um cão
            if (hit.gameObject.tag == "Dog")
            {
                Debug.Log("O DOG N EH MAU!!");


                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Dog = hit.gameObject;

                    //Muda a variavel para dizer que esta carregando
                    Esta_Carregando = true;

                    //Cira uma variavel para o codigo achado no objeto
                    Pegar pegar = Dog.GetComponent<Pegar>();

                    //Executa a função de outro codigo para carregar o dog
                    pegar.Carregar_Dog();

                    GetComponent<ParticleSystem>().Play();
                }
            }
        }
        //Se o jogador NÂO estiver Carregando um objeto
        else
        {
            //Se encostar em uma casa
            if (hit.gameObject.tag == "Casa")
            {
                Casa casa = hit.gameObject.GetComponent<Casa>();

                //Se a colera e a casa forem correspondentes
                if (casa.Cor == Cor)
                {
                    //Cira uma variavel para o codigo achado no objeto
                    Pegar pegar = Dog.GetComponent<Pegar>();

                    //Executa a função de outro codigo para carregar o dog
                    pegar.Devolver_Dog();

                    if (FX_Feliz)
                        FX_Feliz.Play();

                    Esta_Carregando = false;
                }
                //Se NÂO forem correspondentes
                else
                {
                    //Levou uma mordida, menos uma vida.
                    vida--;
                    if (FX_Triste)
                        FX_Triste.Play();
                }
            }
        }



    }
}
