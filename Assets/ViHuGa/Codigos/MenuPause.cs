﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour {

    public static bool JogoPausado = false;
    public GameObject Menu_Pause;

	// Use this for initialization
	void Start () {
        Menu_Pause.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (JogoPausado == true)
            {
                Continuar();
            }
            else
            {
                Pause();
            }
        }
	}

    public void Continuar()
    {
        Menu_Pause.SetActive(false);
        Time.timeScale = 1f;
        JogoPausado = false;

    }
    public void Pause()
    {
        Menu_Pause.SetActive(true);
        Time.timeScale = 0f;
        JogoPausado = true;
    }
    public void Sair_Jogo()
    {
        Application.Quit();
    }
    public void Menu_Principal()
    {
        SceneManager.LoadScene("Menu");
        Menu_Pause.SetActive(false);
        Time.timeScale = 1f;
        JogoPausado = false;
    }
}
