﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Jogador : MonoBehaviour {

    public CharacterController cc;
    public float velocidade = 10;
    public Vector3 movimento;



	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();	
	}
	
	// Update is called once per frame
	void Update () {
		
        
        if (Input.GetKeyDown(KeyCode.W))
        {
            //transform.Rotate(0, 0, 90);

            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, new Vector3(0, 270, 0), 100 * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {

            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, new Vector3(0, 90, 0), 100 * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {

            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, new Vector3(0, 180, 0), 100 * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {

            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, new Vector3(0, 0, 0), 100 * Time.deltaTime);
        }

        movimento = new Vector3(Input.GetAxis("Horizontal") * velocidade / 2, movimento.y, Input.GetAxis("Vertical") * velocidade);
        cc.SimpleMove(movimento);
    }
}
